import 'package:json_annotation/json_annotation.dart';

part 'book.g.dart';

@JsonSerializable(explicitToJson: true)
class Book{
  int id;
  String data;

  Book({this.id, this.data});

  factory Book.fromJson(Map<String, dynamic> json) => _$BookFromJson(json);

  Map<String, dynamic> toJson() => _$BookToJson(this);

  Map<String, Object> toMap() {
    var map = <String, dynamic>{
      "id": id,
      "data": data
    };
    return map;
  }

  Book.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    data = map['data'];
  }
}