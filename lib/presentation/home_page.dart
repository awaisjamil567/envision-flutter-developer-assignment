import 'package:flutter/material.dart';
import 'package:practices/presentation/capture.dart';
import 'package:practices/presentation/library.dart';

class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: TabBar(
            tabs: [
              Tab(text: 'CAPTURE',),
              Tab(text: 'LIBRARY',),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            CapturePage(),
            LibraryPage(),
          ],
        ),
      ),
    );
  }
}
