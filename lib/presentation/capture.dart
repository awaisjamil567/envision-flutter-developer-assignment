import 'package:flutter/material.dart';
import 'package:flutter_mobile_vision/flutter_mobile_vision.dart';
import 'package:practices/data/book.dart';
import 'package:practices/localStorage/local_storage.dart';

class CapturePage extends StatefulWidget {
  const CapturePage({Key key}) : super(key: key);

  @override
  _CapturePageState createState() => _CapturePageState();
}

class _CapturePageState extends State<CapturePage> {

  var _textValue = "";

  var dbHelper = LocalStorage.instance;
  
  insertBook(Book book) async {
    var resposnse =  await dbHelper.insert(book);
  }
  
  Future<Null> _read() async {
    List<OcrText> texts = [];
    try {
      texts = await FlutterMobileVision.read(
        camera: FlutterMobileVision.CAMERA_BACK,
        waitTap: true,
      );

      setState(() {
        _textValue = texts[0].value; // Getting first text block....
      });
    } on Exception {
      texts.add(new OcrText('Failed to recognize text.'));
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(35),
      child: _textValue == "" ? _buildButton(): _buildTextWidget(),
    );
  }

  Widget _buildButton() {
    return Center(
      child: RaisedButton(
        onPressed: () {
          _read();
        },
        child: Text("Scan"),
      ),
    );
  }

  Widget _buildTextWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Text(
            _textValue,
            style: TextStyle(
              color: Colors.black,
              fontSize: 18
            ),
          ),
        ),
        Center(
          child: GestureDetector(
            onTap: () async {
              await insertBook(Book(data: _textValue));
              _showToast(context);
            },
            child: Container(
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: Colors.indigo,
                borderRadius: BorderRadius.circular(25),
              ),
              child: Text("SAVE TEXT TO LIBRARY", style: TextStyle(
                color: Colors.white
              ),),
            ),
          ),
        ),
      ],
    );
  }
  void _showToast(BuildContext context) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: const Text('Text Saved to Library..'),
      ),
    );
  }
}
