import 'package:flutter/material.dart';
import 'package:practices/data/book.dart';
import 'package:practices/localStorage/local_storage.dart';

class LibraryPage extends StatefulWidget {
  const LibraryPage({Key key}) : super(key: key);

  @override
  _LibraryPageState createState() => _LibraryPageState();
}

class _LibraryPageState extends State<LibraryPage> {

  var dbHelper = LocalStorage.instance;
  List<Book> books =[];

  getAllBooks() async {
    var resposnse =  await dbHelper.getAllBooks();
    books = resposnse;
  }

  @override
  void initState() {
    super.initState();
    getAllBooks();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemCount: books.length,
        itemBuilder: (context, index) {
          Container(
            child: Text(
              books[index].data,
              style: TextStyle(
                color: Colors.black,
                fontSize: 20
              ),
            ),
          );
        },
      ),
    );
  }
}
