import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:practices/data/book.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';


class LocalStorage{
  static final _databaseName = "Envision.db";
  static final _databaseVersion = 1;

  static final table = 'my_library';

  static final columnId = 'id';
  static final columnData = 'data';

  LocalStorage._privateConstructor();
  static final LocalStorage instance = LocalStorage._privateConstructor();

  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    // lazily instantiate the db the first time it is accessed
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion,
        onCreate: _onCreate);
  }

  // SQL code to create the database table
  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $table (
            $columnId INTEGER PRIMARY KEY,
            $columnData TEXT NOT NULL
          )
          ''');
  }

  Future<Book> insert(Book book) async {
    Database db = await instance.database;
    book.id = await db.insert(table, book.toMap());
    return book;
  }

  Future<List<Book>> getAllBooks() async {
    Database db = await instance.database;
    List<Book> books = [];
    var maps = await db.query(table);
    maps.forEach((element) {
      books.add(Book.fromMap(element));
    });
    return books;
  }
}