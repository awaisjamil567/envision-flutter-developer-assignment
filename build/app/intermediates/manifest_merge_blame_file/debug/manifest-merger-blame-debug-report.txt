1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.personal.practices"
4    android:versionCode="1"
5    android:versionName="1.0.0" >
6
7    <uses-sdk
8        android:minSdkVersion="21"
9        android:targetSdkVersion="29" />
10    <!--
11         Flutter needs it to communicate with the running application
12         to allow setting breakpoints, to provide hot reload, etc.
13    -->
14    <uses-permission android:name="android.permission.INTERNET" />
14-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:9:5-66
14-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:9:22-64
15    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
15-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:10:5-80
15-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:10:22-77
16    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
16-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:11:5-81
16-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:11:22-78
17    <uses-permission android:name="android.permission.CAMERA" />
17-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:12:5-65
17-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:12:22-62
18
19    <uses-feature android:name="android.hardware.camera" />
19-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:13:5-60
19-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:13:19-57
20    <uses-feature android:name="android.hardware.camera.autofocus" />
20-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:14:5-70
20-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:14:19-67
21
22    <application
22-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:16:5-57:19
23        android:name="io.flutter.app.FlutterApplication"
23-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:17:9-57
24        android:appComponentFactory="androidx.core.app.CoreComponentFactory"
24-->[androidx.core:core:1.1.0] /Users/new/.gradle/caches/transforms-2/files-2.1/21cc2b5dc8f7b552320b6f249a8df9b7/core-1.1.0/AndroidManifest.xml:24:18-86
25        android:debuggable="true"
26        android:icon="@mipmap/ic_launcher"
26-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:19:9-43
27        android:label="Envision Flutter" >
27-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:18:9-41
28        <activity
28-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:20:9-48:20
29            android:name="com.personal.practices.MainActivity"
29-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:21:13-41
30            android:configChanges="orientation|keyboardHidden|keyboard|screenSize|smallestScreenSize|locale|layoutDirection|fontScale|screenLayout|density|uiMode"
30-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:24:13-163
31            android:hardwareAccelerated="true"
31-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:25:13-47
32            android:launchMode="singleTop"
32-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:22:13-43
33            android:theme="@style/LaunchTheme"
33-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:23:13-47
34            android:windowSoftInputMode="adjustResize" >
34-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:26:13-55
35
36            <!--
37                 Specifies an Android theme to apply to this Activity as soon as
38                 the Android process has started. This theme is visible to the user
39                 while the Flutter UI initializes. After that, this theme continues
40                 to determine the Window background behind the Flutter UI.
41            -->
42            <meta-data
42-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:31:13-34:17
43                android:name="io.flutter.embedding.android.NormalTheme"
43-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:32:15-70
44                android:resource="@style/NormalTheme" />
44-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:33:15-52
45            <!--
46                 Displays an Android View that continues showing the launch screen
47                 Drawable until Flutter paints its first frame, then this splash
48                 screen fades out. A splash screen is useful to avoid any visual
49                 gap between the end of Android's launch screen and the painting of
50                 Flutter's first frame.
51            -->
52            <meta-data
52-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:40:13-43:17
53                android:name="io.flutter.embedding.android.SplashScreenDrawable"
53-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:41:15-79
54                android:resource="@drawable/launch_background" />
54-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:42:15-61
55
56            <intent-filter>
56-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:44:13-47:29
57                <action android:name="android.intent.action.MAIN" />
57-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:45:17-68
57-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:45:25-66
58
59                <category android:name="android.intent.category.LAUNCHER" />
59-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:46:17-76
59-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:46:27-74
60            </intent-filter>
61        </activity>
62        <activity android:name="io.github.edufolly.fluttermobilevision.ocr.OcrCaptureActivity" />
62-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:50:9-98
62-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:50:19-95
63
64        <!--
65             Don't delete the meta-data below.
66             This is used by the Flutter tool to generate GeneratedPluginRegistrant.java
67        -->
68        <meta-data
68-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:54:9-56:33
69            android:name="flutterEmbedding"
69-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:55:13-44
70            android:value="2" />
70-->/Users/new/Documents/envision-flutter-developer-assignment/android/app/src/main/AndroidManifest.xml:56:13-30
71
72        <activity
72-->[com.google.android.gms:play-services-base:17.0.0] /Users/new/.gradle/caches/transforms-2/files-2.1/021eddd45250ff6b621f35ebebb75c3f/play-services-base-17.0.0/AndroidManifest.xml:23:9-26:75
73            android:name="com.google.android.gms.common.api.GoogleApiActivity"
73-->[com.google.android.gms:play-services-base:17.0.0] /Users/new/.gradle/caches/transforms-2/files-2.1/021eddd45250ff6b621f35ebebb75c3f/play-services-base-17.0.0/AndroidManifest.xml:24:13-79
74            android:exported="false"
74-->[com.google.android.gms:play-services-base:17.0.0] /Users/new/.gradle/caches/transforms-2/files-2.1/021eddd45250ff6b621f35ebebb75c3f/play-services-base-17.0.0/AndroidManifest.xml:25:13-37
75            android:theme="@android:style/Theme.Translucent.NoTitleBar" />
75-->[com.google.android.gms:play-services-base:17.0.0] /Users/new/.gradle/caches/transforms-2/files-2.1/021eddd45250ff6b621f35ebebb75c3f/play-services-base-17.0.0/AndroidManifest.xml:26:13-72
76
77        <meta-data
77-->[com.google.android.gms:play-services-basement:17.0.0] /Users/new/.gradle/caches/transforms-2/files-2.1/185c6891d637cf751ac0ed55f88c1628/jetified-play-services-basement-17.0.0/AndroidManifest.xml:23:9-25:69
78            android:name="com.google.android.gms.version"
78-->[com.google.android.gms:play-services-basement:17.0.0] /Users/new/.gradle/caches/transforms-2/files-2.1/185c6891d637cf751ac0ed55f88c1628/jetified-play-services-basement-17.0.0/AndroidManifest.xml:24:13-58
79            android:value="@integer/google_play_services_version" />
79-->[com.google.android.gms:play-services-basement:17.0.0] /Users/new/.gradle/caches/transforms-2/files-2.1/185c6891d637cf751ac0ed55f88c1628/jetified-play-services-basement-17.0.0/AndroidManifest.xml:25:13-66
80    </application>
81
82</manifest>
